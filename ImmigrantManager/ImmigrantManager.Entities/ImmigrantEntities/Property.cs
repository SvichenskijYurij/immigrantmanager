﻿namespace ImmigrantManager.Entities.ImmigrantEntities
{
    public class Property
    {
        public bool Weapons { get; set; }
        public bool HazardMatherials { get; set; }

        public Property()
        {
            Weapons = false;
            HazardMatherials = false;
        }
    }
}
