﻿namespace ImmigrantManager.Entities.ImmigrantEntities
{
    public class Identity
    {
        public int Age { get; set; }
        public string FullName { get; set; }

        public Identity()
        {
            Age = 18;
            FullName = "John Doe";
        }
    }
}
