﻿namespace ImmigrantManager.Entities.ImmigrantEntities
{
    public class Education
    {
        private bool _primary;

        public bool Primary
        {
            get { return _primary; }
            set { _primary = value; }
        }

        private bool _pofessional;

        public bool Professional
        {
            get { return _pofessional; }
            set
            {
                _pofessional = value;
                if (value)
                    Primary = true;
            }
        }

        private bool _higher;

        public bool Higher
        {
            get { return _higher; }
            set
            {
                _higher = value;
                if (value)
                    Professional = true;
            }
        }

        public Education()
        {
            Primary = false;
            Professional = false;
            Higher = false;
        }
    }
}
