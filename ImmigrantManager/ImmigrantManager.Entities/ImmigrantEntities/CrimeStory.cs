﻿namespace ImmigrantManager.Entities.ImmigrantEntities
{
    public class CrimeStory
    {
        private int _yearsOfJail;

        public int YearsOfJail
        {
            get { return _yearsOfJail; }
            set { _yearsOfJail = value; }
        }

        private int _arrestsCount;

        public int ArrestsCount
        {
            get { return _arrestsCount; }
            set
            {
                _arrestsCount = value;
                if (value == 0)
                    YearsOfJail = 0;
            }
        }


        public CrimeStory()
        {
            YearsOfJail = 0;
            ArrestsCount = 0;
        }
    }
}
