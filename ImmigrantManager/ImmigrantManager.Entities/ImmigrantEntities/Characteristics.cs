﻿namespace ImmigrantManager.Entities.ImmigrantEntities
{
    public class Characteristics
    {
        public bool Scars { get; set; }
        public bool Tatooes { get; set; }

        public Characteristics()
        {
            Scars = false;
            Tatooes = false;
        }
    }
}
