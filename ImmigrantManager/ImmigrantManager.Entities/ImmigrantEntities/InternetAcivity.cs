﻿namespace ImmigrantManager.Entities.ImmigrantEntities
{
    public class InternetAcivity
    {
        public int AccountsCount { get; set; }
        public bool SuspiciousActivity { get; set; }

        public InternetAcivity()
        {
            AccountsCount = 0;
            SuspiciousActivity = false;
        }
    }
}
