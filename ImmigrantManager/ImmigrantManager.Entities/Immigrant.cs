﻿using ImmigrantManager.Entities.ImmigrantEntities;

namespace ImmigrantManager.Entities
{
    public class Immigrant
    {
        public Characteristics Characteristics { get; set; }
        public CrimeStory CrimeStory { get; set; }
        public Education Education { get; set; }
        public Identity Identity { get; set; }
        public InternetAcivity InternetAcivity { get; set; }
        public Property Property { get; set; }

        public Immigrant()
        {
            Characteristics = new Characteristics();
            CrimeStory = new CrimeStory();
            Education = new Education();
            Identity = new Identity();
            InternetAcivity = new InternetAcivity();
            Property = new Property();
        }
    }
}
