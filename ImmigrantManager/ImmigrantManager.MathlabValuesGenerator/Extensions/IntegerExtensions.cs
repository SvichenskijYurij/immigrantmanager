﻿using System;

namespace ImmigrantManager.MathlabValuesGenerator.Extensions
{
    public static class IntegerExtensions
    {
        public static int Random(this int param)
        {
            var random = new Random();
            var value = random.Next(0, 100);
            return value;
        }
    }
}
