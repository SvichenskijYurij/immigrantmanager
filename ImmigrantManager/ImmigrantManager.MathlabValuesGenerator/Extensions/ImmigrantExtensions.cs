﻿using System;
using ImmigrantManager.Entities;
using ImmigrantManager.MathlabValuesGenerator.Entities;

namespace ImmigrantManager.MathlabValuesGenerator.Extensions
{
    public static class ImmigrantExtensions
    {
        public static MathlabValues ToMathlabValues(this Immigrant immigrant)
        {
            var mathlabValues = new MathlabValues();

            mathlabValues.InputsCharacteristics.Add(Convert.ToInt32(immigrant.Characteristics.Scars));
            mathlabValues.InputsCharacteristics.Add(Convert.ToInt32(immigrant.Characteristics.Tatooes));
            mathlabValues.InputsCharacteristics.Add(0);

            mathlabValues.InputsCrimeStory.Add(immigrant.CrimeStory.YearsOfJail);
            mathlabValues.InputsCrimeStory.Add(immigrant.CrimeStory.ArrestsCount);
            mathlabValues.InputsCrimeStory.Add(0);

            mathlabValues.InputsEducation.Add(Convert.ToInt32(immigrant.Education.Primary));
            mathlabValues.InputsEducation.Add(Convert.ToInt32(immigrant.Education.Professional));
            mathlabValues.InputsEducation.Add(Convert.ToInt32(immigrant.Education.Higher));

            mathlabValues.InputsIdenity.Add(immigrant.Identity.Age);
            mathlabValues.InputsIdenity.Add(0);
            mathlabValues.InputsIdenity.Add(0);

            mathlabValues.InputsActivity.Add(immigrant.InternetAcivity.AccountsCount);
            mathlabValues.InputsActivity.Add(Convert.ToInt32(immigrant.InternetAcivity.SuspiciousActivity));
            mathlabValues.InputsActivity.Add(0);

            mathlabValues.InputsProperty.Add(Convert.ToInt32(immigrant.Property.Weapons));
            mathlabValues.InputsProperty.Add(Convert.ToInt32(immigrant.Property.HazardMatherials));
            mathlabValues.InputsProperty.Add(0);

            return mathlabValues;
        }
    }
}
