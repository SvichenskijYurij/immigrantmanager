﻿using System;

namespace ImmigrantManager.MathlabValuesGenerator.Extensions
{
    public static class BooleanExtension
    {
        public static bool Random(this bool param)
        {
            var random = new Random();
            var value = random.Next(2);
            return Convert.ToBoolean(value);
        }
    }
}
