﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImmigrantManager.MathlabValuesGenerator.Entities
{
    public class MathlabValues
    {
        public List<bool> IsAccepted { get; set; }
        public List<int> InputsCharacteristics { get; set; }
        public List<int> InputsCrimeStory { get; set; }
        public List<int> InputsEducation { get; set; }
        public List<int> InputsIdenity { get; set; }
        public List<int> InputsActivity { get; set; }
        public List<int> InputsProperty { get; set; }

        public MathlabValues()
        {
            IsAccepted = new List<bool>();
            InputsCharacteristics = new List<int>();
            InputsCrimeStory = new List<int>();
            InputsEducation = new List<int>();
            InputsIdenity = new List<int>();
            InputsActivity = new List<int>();
            InputsProperty = new List<int>();
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("Inputs = ");
            stringBuilder.Append('{');
            for (var i = 0; i < 3; i++)
            {
                stringBuilder.Append(' ');
                var characteristic = InputsCharacteristics[i];
                var identity = InputsIdenity[i];
                var crime = InputsCrimeStory[i];
                var education = InputsEducation[i];
                var activity = InputsActivity[i]; 
                var property = InputsProperty[i];

                stringBuilder.Append($" {identity} ");
                stringBuilder.Append($" {property} ");
                stringBuilder.Append($" {characteristic} ");
                stringBuilder.Append($" {education} ");
                stringBuilder.Append($" {activity} ");
                stringBuilder.Append($" {crime} ");
                if (i != 2)
                    stringBuilder.Append(';');
            }
            stringBuilder.Append('}');
            stringBuilder.AppendLine();
            stringBuilder.Append("Target = ");
            stringBuilder.Append('{');
            foreach (var item in IsAccepted)
            {
                stringBuilder.Append($" {Convert.ToInt32(item)} ");
            }
            stringBuilder.Append('}');
            return stringBuilder.ToString();
        }
    }
}
