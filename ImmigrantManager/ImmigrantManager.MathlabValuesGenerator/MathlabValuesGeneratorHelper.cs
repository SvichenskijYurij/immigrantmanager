﻿using System.Threading;
using ImmigrantManager.Entities;
using ImmigrantManager.Entities.ImmigrantEntities;
using ImmigrantManager.MathlabValuesGenerator.Entities;
using ImmigrantManager.MathlabValuesGenerator.Extensions;

namespace ImmigrantManager.MathlabValuesGenerator
{
    public class MathlabValuesGeneratorHelper
    {
        //private const int NumberOfCharacteristics = 6;

        public static MathlabValues GenerateValues(ref Immigrant immigrant)
        {
            const int timeToSleep = 1000;

            //var accepts = new bool[NumberOfCharacteristics];
            //var acceptedCount = 0;

            immigrant.Characteristics.Scars = new bool().Random();
            Thread.Sleep(timeToSleep);
            immigrant.Characteristics.Tatooes = new bool().Random();
            Thread.Sleep(timeToSleep);
            immigrant.CrimeStory.YearsOfJail = new int().Random();
            Thread.Sleep(timeToSleep);
            immigrant.CrimeStory.ArrestsCount = new int().Random();
            Thread.Sleep(timeToSleep);
            immigrant.Education.Primary = new bool().Random();
            Thread.Sleep(timeToSleep);
            immigrant.Education.Professional = new bool().Random();
            Thread.Sleep(timeToSleep);
            immigrant.Education.Higher = new bool().Random();
            Thread.Sleep(timeToSleep);
            immigrant.Identity.Age = new int().Random();
            Thread.Sleep(timeToSleep);
            immigrant.InternetAcivity.AccountsCount = new int().Random();
            Thread.Sleep(timeToSleep);
            immigrant.InternetAcivity.SuspiciousActivity = new bool().Random();
            Thread.Sleep(timeToSleep);
            immigrant.Property.HazardMatherials = new bool().Random();
            Thread.Sleep(timeToSleep);
            immigrant.Property.Weapons = new bool().Random();

            var mathlabValues = immigrant.ToMathlabValues();

            mathlabValues.IsAccepted.Add(CheckIdentity(immigrant.Identity));
            mathlabValues.IsAccepted.Add(CheckProperty(immigrant.Property));
            mathlabValues.IsAccepted.Add(CheckCharacteristics(immigrant.Characteristics));
            mathlabValues.IsAccepted.Add(CheckEducation(immigrant.Education, immigrant.Identity));
            mathlabValues.IsAccepted.Add(CheckInternetActivity(immigrant.InternetAcivity));
            mathlabValues.IsAccepted.Add(CheckCrimeStory(immigrant.CrimeStory));

            //for (var i = 0; i < NumberOfCharacteristics; i++)
            //{
            //    var item = accepts[i];

            //    if (item)
            //        acceptedCount++;
            //}

            //if (acceptedCount < 4)
            //    mathlabValues.IsAccepted = false;

            return mathlabValues;
        }

        private static bool CheckCharacteristics(Characteristics characteristics)
        {
            var isAlloved = !(characteristics.Scars && characteristics.Tatooes);
            return isAlloved;
        }

        private static bool CheckCrimeStory(CrimeStory crimeStory)
        {
            if (crimeStory.YearsOfJail > 3)
                return false;
            return crimeStory.ArrestsCount <= 1;
        }

        private static bool CheckEducation(Education education, Identity identity)
        {
            if (identity.Age <= 21 && education.Primary)
                return true;

            return identity.Age > 25 || !education.Primary || education.Professional;
        }

        private static bool CheckIdentity(Identity identity)
        {
            return identity.Age <= 50;
        }

        private static bool CheckInternetActivity(InternetAcivity internetAcivity)
        {
            if (internetAcivity.AccountsCount >= 10 || internetAcivity.AccountsCount <= 0)
                return false;

            return !internetAcivity.SuspiciousActivity;
        }

        private static bool CheckProperty(Property property)
        {
            if (property.HazardMatherials && property.Weapons)
                return false;

            return !property.HazardMatherials || property.Weapons;
        }
    }
}
