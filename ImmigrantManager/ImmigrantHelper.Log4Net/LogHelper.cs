﻿using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using log4net;
using log4net.Appender;

namespace ImmigrantHelper.Log4Net
{
    public static class LogHelper
    {
        public static ILog GetLogger([CallerFilePath] string filename = "")
        {
            return LogManager.GetLogger(filename);
        }

        //Todo investigate this!!!!
        public static string GetLogsFileSize(ILog log)
        {
            var path = GetLogsFilePath(log);
            if (!File.Exists(path)) return "0 Bytes";
            if (path == null) return "0 Bytes";
            var fInf = new FileInfo(path);

            string sLen;

            if (fInf.Length >= 1 << 30)
                sLen = $"{fInf.Length >> 30} Gb";
            else if (fInf.Length >= 1 << 20)
                sLen = $"{fInf.Length >> 20} Mb";
            else if (fInf.Length >= 1 << 10)
                sLen = $"{fInf.Length >> 10} Kb";
            else
                sLen = $"{fInf.Length} Bytes";

            return sLen;
        }

        public static string GetLogsFilePath(ILog log)
        {
            var path = log.Logger.Repository.GetAppenders().OfType<FileAppender>().FirstOrDefault()?.File;
            return path;
        }
    }
}
