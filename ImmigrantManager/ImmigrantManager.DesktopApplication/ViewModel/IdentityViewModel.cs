﻿using ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class IdentityViewModel
    {
        public IdentityViewModel()
        {
            Model = new Identity();
        }

        public Identity Model { get; set; }
    }
}
