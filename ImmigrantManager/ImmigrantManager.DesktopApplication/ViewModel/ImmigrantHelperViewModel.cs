﻿using System.Windows.Input;
using ImmigrantManager.DesktopApplication.Command;
using ImmigrantManager.DesktopApplication.Model;
using ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class ImmigrantHelperViewModel
    {
        public ImmigrantHelperViewModel()
        {
            Model = new ImmigrantHelperModel();
            GetResultsCommand = new RelayCommand(GetResults);
        }

        public ImmigrantHelperModel Model { get; set; }

        public ICommand GetResultsCommand { get; set; }

        private void GetResults(object param)
        {
            //ToDo: Set logic for getting the results
        }
    }
}
