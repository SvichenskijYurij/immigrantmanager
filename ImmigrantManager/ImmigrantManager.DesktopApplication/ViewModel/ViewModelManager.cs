﻿namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class ViewModelManager
    {
        private static ViewModelManager _instance;

        public ValueGeneratorViewModel ValueGeneratorViewModel { get; private set; }
        public MainWindowViewModel MainWindowViewModel { get; private set; }
        public ImmigrantHelperViewModel ImmigrantHelperViewModel { get; private set; }
        public CharacteristicsViewModel CharacteristicsViewModel { get; private set; }
        public CrimeStoryViewModel CrimeStoryViewModel { get; private set; }
        public EducationViewModel EducationViewModel { get; private set; }
        public IdentityViewModel IdentityViewModel { get; private set; }
        public InternetActivityViewModel InternetActivityViewModel { get; private set; }
        public PropertyViewModel PropertyViewModel { get; private set; }
        public NetworkTrainerViewModel NetworkTrainerViewModel { get; private set; }

        private ViewModelManager()
        {
            MainWindowViewModel = new MainWindowViewModel();
            ValueGeneratorViewModel = new ValueGeneratorViewModel();
            ImmigrantHelperViewModel = new ImmigrantHelperViewModel();
            CharacteristicsViewModel = new CharacteristicsViewModel();
            CrimeStoryViewModel = new CrimeStoryViewModel();
            EducationViewModel = new EducationViewModel();
            IdentityViewModel = new IdentityViewModel();
            InternetActivityViewModel = new InternetActivityViewModel();
            PropertyViewModel = new PropertyViewModel();
            NetworkTrainerViewModel = new NetworkTrainerViewModel();
        }

        public static ViewModelManager GetInstance()
        {
            return _instance ?? (_instance = new ViewModelManager());
        }
    }
}
