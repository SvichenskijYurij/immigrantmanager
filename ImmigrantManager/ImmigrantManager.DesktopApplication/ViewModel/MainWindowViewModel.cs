﻿using ImmigrantManager.DesktopApplication.Model;
using ToastNotifications;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel()
        {
            Model = new MainWindowModel();
        }

        public MainWindowModel Model { get; set; }

        public void ShowInformation(string message)
        {
            Model.NotificationSource.Show(message, NotificationType.Information);
        }

        public void ShowSuccess(string message)
        {
            Model.NotificationSource.Show(message, NotificationType.Success);
        }

        public void ShowWarning(string message)
        {
            Model.NotificationSource.Show(message, NotificationType.Warning);
        }

        public void ShowError(string message)
        {
            Model.NotificationSource.Show(message, NotificationType.Error);
        }
    }
}
