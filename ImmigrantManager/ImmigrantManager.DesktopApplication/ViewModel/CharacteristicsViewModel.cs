﻿using ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class CharacteristicsViewModel
    {
        public CharacteristicsViewModel()
        {
            Model = new Characteristics();
        }

        public Characteristics Model { get; set; }
    }
}
