﻿using ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class CrimeStoryViewModel
    {
        public CrimeStoryViewModel()
        {
            Model = new CrimeStory();
        }

        public CrimeStory Model { get; set; }
    }
}
