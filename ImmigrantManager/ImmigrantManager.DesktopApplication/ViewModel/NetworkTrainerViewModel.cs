﻿using System;
using System.Windows.Input;
using ImmigrantManager.DesktopApplication.Command;
using ImmigrantManager.DesktopApplication.Model;
using ImmigrantManager.Entities;
using ImmigrantManager.MathlabValuesGenerator;
using MLNetWrapper;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class NetworkTrainerViewModel
    {
        private Net _net;
        public NetworkTrainerModel Model { get; set; }

        public NetworkTrainerViewModel()
        {
            Model = new NetworkTrainerModel();
            TrainCommand = new RelayCommand(Train);
        }

        public ICommand TrainCommand { get; set; }

        private void Train(object param)
        {
            var immigrant = new Immigrant();
            var mathlabValues = MathlabValuesGeneratorHelper.GenerateValues(ref immigrant);
            var input = new double[,]
            {
                {mathlabValues.InputsActivity[0], mathlabValues.InputsActivity[1], mathlabValues.InputsActivity[2]},
                {mathlabValues.InputsCharacteristics[0], mathlabValues.InputsCharacteristics[1], mathlabValues.InputsCharacteristics[2]},
                {mathlabValues.InputsCrimeStory[0], mathlabValues.InputsCrimeStory[1], mathlabValues.InputsCrimeStory[2]},
                {mathlabValues.InputsEducation[0], mathlabValues.InputsEducation[1], mathlabValues.InputsEducation[2]},
                {mathlabValues.InputsIdenity[0], mathlabValues.InputsIdenity[1], mathlabValues.InputsIdenity[2]},
                {mathlabValues.InputsProperty[0], mathlabValues.InputsProperty[1], mathlabValues.InputsProperty[2]},
            };
            var output = new double[,]
            {
                {Convert.ToInt32(mathlabValues.IsAccepted[0]) },
                {Convert.ToInt32(mathlabValues.IsAccepted[1]) },
                {Convert.ToInt32(mathlabValues.IsAccepted[2]) },
                {Convert.ToInt32(mathlabValues.IsAccepted[3]) },
                {Convert.ToInt32(mathlabValues.IsAccepted[4]) },
                {Convert.ToInt32(mathlabValues.IsAccepted[5]) }
            };
            var wrapper = new Wrapper();
            _net = new Net(wrapper, 3, 6);
            _net.Train(input, output);
            var result = _net.Execute(new double[] {  mathlabValues.InputsActivity[0], mathlabValues.InputsActivity[1], mathlabValues.InputsActivity[2] });
            App.Manager.MainWindowViewModel.ShowInformation("Network trained.");
        }
    }
}
