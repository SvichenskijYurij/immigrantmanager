﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using ImmigrantHelper.Log4Net;
using ImmigrantManager.DesktopApplication.Command;
using ImmigrantManager.DesktopApplication.Model;
using ImmigrantManager.Entities;
using ImmigrantManager.MathlabValuesGenerator;
using Microsoft.Win32;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class ValueGeneratorViewModel
    {
        private static readonly log4net.ILog Log = LogHelper.GetLogger();

        public ValueGeneratorViewModel()
        {
            Model = new ValueGeneratorModel();
            GenerateValuesCommand = new RelayCommand(GenerateValues);
            DownloadValuesCommand = new RelayCommand(DownloadValues);
            ClearValuesCommand = new RelayCommand(ClearValues);
            ShowValues();
        }

        public ValueGeneratorModel Model { get; set; }

        public ICommand GenerateValuesCommand { get; set; }
        public ICommand DownloadValuesCommand { get; set; }
        public ICommand ClearValuesCommand { get; set; }

        private void GenerateValues(object param)
        {
            if (Model.NumberOfValues <= 0)
            {
                App.Manager.MainWindowViewModel.ShowWarning("Wrong number of values.");
                return;
            }
            App.Manager.MainWindowViewModel.Model.IsPerformingOperation = true;
            Task.Factory.StartNew(() =>
            {
                for (var i = 0; i < Model.NumberOfValues; i++)
                {
                    var immigrant = new Immigrant();
                    var mathlabValues = MathlabValuesGeneratorHelper.GenerateValues(ref immigrant);
                    Log.Info(mathlabValues.ToString());
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    App.Manager.MainWindowViewModel.ShowSuccess("New values were generated!");
                    App.Manager.MainWindowViewModel.Model.IsPerformingOperation = false;
                    ShowValues();
                }));
            });
        }

        private void DownloadValues(object param)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "TXT file (*.txt)|*.txt",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Title = "Save values",
                FileName = "MathlabValues"
            };
            var path = LogHelper.GetLogsFilePath(Log);
            if (File.Exists(path))
            {
                saveFileDialog.ShowDialog();
                File.Copy(path, saveFileDialog.FileName, true);
                ShowValues();
            }
            else
            {
                App.Manager.MainWindowViewModel.ShowInformation("File hasn't been created yet.");
            }
        }

        private void ClearValues(object param)
        {
            var path = LogHelper.GetLogsFilePath(Log);
            if (File.Exists(path))
                File.Delete(path);
            Model.TextValues = string.Empty;
        }

        private void ShowValues()
        {
            var path = LogHelper.GetLogsFilePath(Log);
            if (File.Exists(path))
            {
                Model.TextValues = File.ReadAllText(path);
            }
        }
    }
}
