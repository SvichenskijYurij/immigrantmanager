﻿using ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class PropertyViewModel
    {
        public PropertyViewModel()
        {
            Model = new Property();
        }

        public Property Model { get; set; }
    }
}
