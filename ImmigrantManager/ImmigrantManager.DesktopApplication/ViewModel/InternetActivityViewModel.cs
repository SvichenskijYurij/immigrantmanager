﻿using ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class InternetActivityViewModel
    {
        public InternetActivityViewModel()
        {
            Model = new InternetAcivity();
        }

        public InternetAcivity Model { get; set; }
    }
}
