﻿using ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities;

namespace ImmigrantManager.DesktopApplication.ViewModel
{
    public class EducationViewModel
    {
        public EducationViewModel()
        {
            Model = new Education();
        }

        public Education Model { get; set; }
    }
}
