﻿using ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities;

namespace ImmigrantManager.DesktopApplication.Model
{
    public class ImmigrantHelperModel : ModelBase
    {
        public ImmigrantHelperModel()
        {
            Immigrant = new Immigrant();
        }

        private Immigrant _immigrant;

        public Immigrant Immigrant
        {
            get { return _immigrant; }
            set
            {
                _immigrant = value;
                OnPropertyChanged();
            }
        }

    }
}
