﻿namespace ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities
{
    public class InternetAcivity : ModelBase
    {
        private int _accountsCount;
        private bool _suspiciousActivity;

        public int AccountsCount
        {
            get { return _accountsCount; }
            set
            {
                _accountsCount = value;
                OnPropertyChanged();
            }
        }

        public bool SuspiciousActivity
        {
            get { return _suspiciousActivity; }
            set
            {
                _suspiciousActivity = value;
                OnPropertyChanged();
            }
        }

        public InternetAcivity()
        {
            AccountsCount = 0;
            SuspiciousActivity = false;
        }
    }
}
