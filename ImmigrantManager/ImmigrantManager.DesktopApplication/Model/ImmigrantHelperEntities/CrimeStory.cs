﻿namespace ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities
{
    public class CrimeStory : ModelBase
    {
        private int _yearsOfJail;

        public int YearsOfJail
        {
            get { return _yearsOfJail; }
            set
            {
                _yearsOfJail = value;
                OnPropertyChanged();
            }
        }

        private int _arrestsCount;

        public int ArrestsCount
        {
            get { return _arrestsCount; }
            set
            {
                _arrestsCount = value;
                if (value == 0)
                    YearsOfJail = 0;
                OnPropertyChanged();
            }
        }


        public CrimeStory()
        {
            YearsOfJail = 0;
            ArrestsCount = 0;
        }
    }
}
