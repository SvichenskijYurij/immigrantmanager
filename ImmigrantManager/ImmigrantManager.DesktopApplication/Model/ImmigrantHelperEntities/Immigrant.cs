﻿namespace ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities
{
    public class Immigrant : ModelBase
    {
        private Characteristics _characteristics;
        private CrimeStory _crimeStory;
        private Education _education;
        private Identity _identity;
        private InternetAcivity _internetAcivity;
        private Property _property;

        public Characteristics Characteristics
        {
            get { return _characteristics; }
            set
            {
                _characteristics = value;
                OnPropertyChanged();
            }
        }

        public CrimeStory CrimeStory
        {
            get { return _crimeStory; }
            set
            {
                _crimeStory = value;
                OnPropertyChanged();
            }
        }

        public Education Education
        {
            get { return _education; }
            set
            {
                _education = value;
                OnPropertyChanged();
            }
        }

        public Identity Identity
        {
            get { return _identity; }
            set
            {
                _identity = value;
                OnPropertyChanged();
            }
        }

        public InternetAcivity InternetAcivity
        {
            get { return _internetAcivity; }
            set
            {
                _internetAcivity = value;
                OnPropertyChanged();
            }
        }

        public Property Property
        {
            get { return _property; }
            set
            {
                _property = value;
                OnPropertyChanged();
            }
        }

        public Immigrant()
        {
            Characteristics = new Characteristics();
            CrimeStory = new CrimeStory();
            Education = new Education();
            Identity = new Identity();
            InternetAcivity = new InternetAcivity();
            Property = new Property();
        }
    }
}
