﻿namespace ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities
{
    public class Property : ModelBase
    {
        private bool _weapons;
        private bool _hazardMatherials;

        public bool Weapons
        {
            get { return _weapons; }
            set
            {
                _weapons = value;
                OnPropertyChanged();
            }
        }

        public bool HazardMatherials
        {
            get { return _hazardMatherials; }
            set
            {
                _hazardMatherials = value;
                OnPropertyChanged();
            }
        }

        public Property()
        {
            Weapons = false;
            HazardMatherials = false;
        }
    }
}
