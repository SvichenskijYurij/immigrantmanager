﻿namespace ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities
{
    public class Characteristics : ModelBase
    {
        private bool _scars;
        private bool _tatooes;

        public bool Scars
        {
            get { return _scars; }
            set
            {
                _scars = value;
                OnPropertyChanged();
            }
        }

        public bool Tatooes
        {
            get { return _tatooes; }
            set
            {
                _tatooes = value;
                OnPropertyChanged();
            }
        }

        public Characteristics()
        {
            Scars = false;
            Tatooes = false;
        }
    }
}
