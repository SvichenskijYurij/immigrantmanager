﻿namespace ImmigrantManager.DesktopApplication.Model.ImmigrantHelperEntities
{
    public class Education : ModelBase
    {
        private bool _primary;

        public bool Primary
        {
            get { return _primary; }
            set
            {
                _primary = value;
                OnPropertyChanged();
            }
        }

        private bool _pofessional;

        public bool Professional
        {
            get { return _pofessional; }
            set
            {
                _pofessional = value;
                if (value)
                    Primary = true;
                OnPropertyChanged();
            }
        }

        private bool _higher;

        public bool Higher
        {
            get { return _higher; }
            set
            {
                _higher = value;
                if (value)
                    Professional = true;
                OnPropertyChanged();
            }
        }

        public Education()
        {
            Primary = false;
            Professional = false;
            Higher = false;
        }

    }
}
