﻿using ToastNotifications;

namespace ImmigrantManager.DesktopApplication.Model
{
    public class MainWindowModel : ModelBase
    {
        public MainWindowModel()
        {
            NotificationSource = new NotificationsSource
            {
                MaximumNotificationCount = 1,
            };

            IsPerformingOperation = false;
        }

        private bool _isPerformingOperation;

        public bool IsPerformingOperation
        {
            get { return _isPerformingOperation; }
            set
            {
                _isPerformingOperation = value;
                OnPropertyChanged();
            }
        }

        private NotificationsSource _notificationSource;

        public NotificationsSource NotificationSource
        {
            get { return _notificationSource; }
            set
            {
                _notificationSource = value;
                OnPropertyChanged();
            }
        }
    }
}
