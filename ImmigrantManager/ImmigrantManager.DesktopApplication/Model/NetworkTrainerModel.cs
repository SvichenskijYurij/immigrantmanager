﻿namespace ImmigrantManager.DesktopApplication.Model
{
    public class NetworkTrainerModel : ModelBase
    {
        public NetworkTrainerModel()
        {
            Inputs = string.Empty;
            Outputs = string.Empty;
        }

        private string _inputs;

        public string Inputs
        {
            get { return _inputs; }
            set
            {
                _inputs = value;
                OnPropertyChanged();
            }
        }

        private string _outputs;

        public string Outputs
        {
            get { return _outputs; }
            set
            {
                _outputs = value;
                OnPropertyChanged();
            }
        }

    }
}
