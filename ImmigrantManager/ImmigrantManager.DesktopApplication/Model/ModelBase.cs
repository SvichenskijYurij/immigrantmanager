﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using ImmigrantManager.DesktopApplication.Annotations;

namespace ImmigrantManager.DesktopApplication.Model
{
    public class ModelBase : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        private readonly Dictionary<string, List<string>> _errorsDictionary;

        public ModelBase()
        {
            _errorsDictionary = new Dictionary<string, List<string>>();
        }

        public bool HasErrors
        {
            get
            {
                return _errorsDictionary.Any(item => item.Value.Count > 0);
            }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            ValidateProperty(propertyName);
        }

        public void ValidateProperty([CallerMemberName] string propertyName = null)
        {
            var validationResults = new List<ValidationResult>();

            var validationContext = new ValidationContext(this, null, null) { MemberName = propertyName };
            if (propertyName != null)
            {
                var value = GetType().GetProperty(propertyName).GetValue(this);

                Validator.TryValidateProperty(value, validationContext, validationResults);
            }

            if (propertyName != null && _errorsDictionary.ContainsKey(propertyName))
            {
                _errorsDictionary[propertyName].Clear();
                foreach (var item in validationResults)
                {
                    _errorsDictionary[propertyName].Add(item.ErrorMessage);
                }
            }
            else
            {
                if (propertyName != null)
                {
                    _errorsDictionary.Add(propertyName, new List<string>());
                    foreach (var item in validationResults)
                    {
                        _errorsDictionary[propertyName].Add(item.ErrorMessage);
                    }
                }
            }

            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public IEnumerable GetErrors(string propertyName)
        {
            return _errorsDictionary.ContainsKey(propertyName) ? _errorsDictionary[propertyName] : null;
        }

        public void AddErrorForProperty(string propertyName, string error)
        {
            if (_errorsDictionary.ContainsKey(propertyName))
            {
                _errorsDictionary[propertyName].Clear();
                _errorsDictionary[propertyName].Add(error);
            }
            else
            {
                _errorsDictionary.Add(propertyName, new List<string>());
                _errorsDictionary[propertyName].Add(error);
            }
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public void ClearErrorsForProperty(string propertyName)
        {
            if (_errorsDictionary.ContainsKey(propertyName))
            {
                _errorsDictionary[propertyName].Clear();
            }
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }
    }
}
