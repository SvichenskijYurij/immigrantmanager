﻿namespace ImmigrantManager.DesktopApplication.Model
{
    public class ValueGeneratorModel : ModelBase
    {
        public ValueGeneratorModel()
        {
            NumberOfValues = 0;
            TextValues = string.Empty;
        }

        private int _numberOfValues;

        public int NumberOfValues
        {
            get { return _numberOfValues; }
            set
            {
                _numberOfValues = value;
                OnPropertyChanged();
            }
        }

        private string _textValues;

        public string TextValues
        {
            get { return _textValues; }
            set
            {
                _textValues = value;
                OnPropertyChanged();
            }
        }
    }
}
