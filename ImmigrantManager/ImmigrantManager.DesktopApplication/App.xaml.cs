﻿using System.Windows;
using ImmigrantManager.DesktopApplication.ViewModel;

namespace ImmigrantManager.DesktopApplication
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App
    {
        public static ViewModelManager Manager => ViewModelManager.GetInstance();

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}
