﻿namespace ImmigrantManager.DesktopApplication
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = App.Manager.MainWindowViewModel;
        }
    }
}
