﻿namespace ImmigrantManager.DesktopApplication.View.ValueGenerator
{
    /// <summary>
    /// Логика взаимодействия для ValueGenerator.xaml
    /// </summary>
    public partial class ValueGenerator
    {
        public ValueGenerator()
        {
            InitializeComponent();
            DataContext = App.Manager.ValueGeneratorViewModel;
        }
    }
}
