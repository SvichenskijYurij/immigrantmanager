﻿namespace ImmigrantManager.DesktopApplication.View.ImmigrantHelper
{
    /// <summary>
    /// Interaction logic for Education.xaml
    /// </summary>
    public partial class Education
    {
        public Education()
        {
            InitializeComponent();
            DataContext = App.Manager.EducationViewModel;
        }
    }
}
