﻿namespace ImmigrantManager.DesktopApplication.View.ImmigrantHelper
{
    /// <summary>
    /// Interaction logic for Identity.xaml
    /// </summary>
    public partial class Identity
    {
        public Identity()
        {
            InitializeComponent();
            DataContext = App.Manager.IdentityViewModel;
        }
    }
}
