﻿namespace ImmigrantManager.DesktopApplication.View.ImmigrantHelper
{
    /// <summary>
    /// Логика взаимодействия для ImmigrantHelper.xaml
    /// </summary>
    public partial class ImmigrantHelper
    {
        public ImmigrantHelper()
        {
            InitializeComponent();
            DataContext = App.Manager.ImmigrantHelperViewModel;
        }
    }
}
