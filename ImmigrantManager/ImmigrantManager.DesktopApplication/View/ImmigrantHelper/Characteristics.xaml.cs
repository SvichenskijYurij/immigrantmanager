﻿namespace ImmigrantManager.DesktopApplication.View.ImmigrantHelper
{
    /// <summary>
    /// Interaction logic for Characteristics.xaml
    /// </summary>
    public partial class Characteristics
    {
        public Characteristics()
        {
            InitializeComponent();
            DataContext = App.Manager.CharacteristicsViewModel;
        }
    }
}
