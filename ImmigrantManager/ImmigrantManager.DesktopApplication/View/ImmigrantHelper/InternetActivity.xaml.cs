﻿namespace ImmigrantManager.DesktopApplication.View.ImmigrantHelper
{
    /// <summary>
    /// Interaction logic for InternetActivity.xaml
    /// </summary>
    public partial class InternetActivity
    {
        public InternetActivity()
        {
            InitializeComponent();
            DataContext = App.Manager.InternetActivityViewModel;
        }
    }
}
