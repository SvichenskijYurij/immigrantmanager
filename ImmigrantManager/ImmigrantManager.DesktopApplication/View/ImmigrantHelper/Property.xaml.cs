﻿namespace ImmigrantManager.DesktopApplication.View.ImmigrantHelper
{
    /// <summary>
    /// Interaction logic for Property.xaml
    /// </summary>
    public partial class Property
    {
        public Property()
        {
            InitializeComponent();
            DataContext = App.Manager.PropertyViewModel;
        }
    }
}
