﻿namespace ImmigrantManager.DesktopApplication.View.ImmigrantHelper
{
    /// <summary>
    /// Interaction logic for CrimeStory.xaml
    /// </summary>
    public partial class CrimeStory 
    {
        public CrimeStory()
        {
            InitializeComponent();
            DataContext = App.Manager.CrimeStoryViewModel;
        }
    }
}
