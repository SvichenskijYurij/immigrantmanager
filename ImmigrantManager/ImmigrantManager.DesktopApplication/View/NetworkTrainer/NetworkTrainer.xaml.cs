﻿namespace ImmigrantManager.DesktopApplication.View.NetworkTrainer
{
    /// <summary>
    /// Логика взаимодействия для NetworkTrainer.xaml
    /// </summary>
    public partial class NetworkTrainer
    {
        public NetworkTrainer()
        {
            InitializeComponent();
            DataContext = App.Manager.NetworkTrainerViewModel;
        }
    }
}
